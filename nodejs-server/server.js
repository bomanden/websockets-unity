const WebSocket = require('ws');
 
function noop() {}
 
function heartbeat() {
  this.isAlive = true;
}
 
const wss = new WebSocket.Server({ port: 8080 });

wss.getUniqueID = function () {
	function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4();
};
 
wss.on('connection', function connection(ws) {
  ws.isAlive = true;
	ws.on('pong', heartbeat);
	ws.id =  wss.getUniqueID();

	//console.log("client connected:	",ws.id);

	ws.on('message', function incoming(message) {

		try {
			const msg = JSON.parse(message);
			//let pcSocket = connected_clients.get(msg.PcID);

			switch (msg.EventType) {
				case "update":


					break;


				default:
					//console.log('[default] not Broadcasted : %s', message);
					wss.clients.forEach(function each(client) {
						if (client !== ws && client.readyState === WebSocket.OPEN) {
							client.send(message);
						}
					});

					break;
			}

		} catch(err) {
			//console.error('Error: ',err)
		}
	});

	ws.on('close', function incoming(msg) {
		//console.log('client disconnected:	',ws.id, 'reason:',msg)
	});
	
	
});
 
const interval = setInterval(function ping() {
	
	
	//console.log("ping clients",wss.clients.size);


	wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();
		
    ws.isAlive = false;
    ws.ping(noop);
  });
}, 30000);