using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NativeWebSocket;
using System.Threading;

public class Connection : MonoBehaviour
{
	public string WebsocketEndpoint = "ws://localhost:8080";
	public float ReconnectInterval = 5f;
	WebSocket websocket;

	// Start is called before the first frame update
	async void Start()
	{
		websocket = new WebSocket(WebsocketEndpoint);

		websocket.OnOpen += Websocket_OnOpen;
		websocket.OnError += Websocket_OnError;
		websocket.OnClose += Websocket_OnClose;
		websocket.OnMessage += Websocket_OnMessage;

		await websocket.Connect();
	}

	private void Websocket_OnMessage(byte[] bytes)
	{
		var message = System.Text.Encoding.UTF8.GetString(bytes);
		Debug.Log($"OnMessage! msg:{message}");
	}

	private void Websocket_OnOpen()
	{
		Debug.Log($"Websocket_OnOpen:");
		//SendWebSocketMessage("{\"test\": \"Unity\"}");
	}

	private void Websocket_OnError(string errorMsg)
	{
		Debug.Log($"Websocket_OnError:{errorMsg}");
	}

	private void Websocket_OnClose(WebSocketCloseCode closeCode)
	{
		Debug.Log($"Websocket_OnClose state:{websocket.State} code:{closeCode}");

		if (closeCode == WebSocketCloseCode.Abnormal)
		{
			StartCoroutine(ReConnectWS(ReconnectInterval));
		}

	}
	
	IEnumerator ReConnectWS(float delay)
	{
		yield return new WaitForSeconds(delay);

		Debug.Log($"ReConnectWS state:{websocket.State}");
		_ = websocket.Connect();

		yield return null;
	}

	void SendWebSocketMessage(string message)
	{
		if (websocket.State == WebSocketState.Open)
		{
			// Sending bytes
			//await websocket.Send(new byte[] { 10, 20, 30 });

			// Sending plain text
			_ = websocket.SendText(message);
		}
	}

	private async void OnApplicationQuit()
	{
		websocket.OnOpen -= Websocket_OnOpen;
		websocket.OnError -= Websocket_OnError;
		websocket.OnClose -= Websocket_OnClose;
		websocket.OnMessage -= Websocket_OnMessage;

		Debug.Log($"Websocket_Dispose quit");
		await websocket.Close();
	}
}
